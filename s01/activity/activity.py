# Declaring variables

name = "Jose"
age = 38
occupation = "writer"
movie = "One More Chance"
decimal = 99.6

print(f"I am {name} , and I am {age} years old, I work as a {occupation} , and my rating for {movie} is {decimal} %")

num1, num2, num3 = 50,3,75

print(num1 * num2)
print(num1 < num3)
print(num3 + num2)